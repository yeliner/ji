﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JI
{
    public partial class jisuanqi : Form
    {
        public jisuanqi()
        {
            InitializeComponent();
        }
        int num1, num2;
        int i = 0;
        private void btn0_Click(object sender, EventArgs e)
        {
            booli(btn0.Text);
        }
        private void btn1_Click(object sender, EventArgs e)
        {
            booli(btn1.Text);
        }

        private void booli(string num)
        {
            if (txt2.Text != "")
            {
                if (i == 1)
                {
                    txt1.Text = txt2.Text;
                    txt2.Text = num;
                    i = 0;
                }
                else {
                    txt2.Text = txt2.Text + num;
                }
            }
            else
            {
                txt2.Text = txt2.Text +num;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            booli(btn2.Text);
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            booli(btn3.Text);
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            booli(btn4.Text);
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            booli(btn5.Text);
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            booli(btn6.Text);
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            booli(btn7.Text);
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            booli(btn8.Text);
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            booli(btn9.Text);
        }
        private void btndian_Click(object sender, EventArgs e)
        {
            if (txt2.Text != "")
            { 
                if (i == 1)
                {
                    txt1.Text = txt2.Text;
                    txt2.Text = 0 + btndian.Text;
                    i = 0;
                }
                string last = txt2.Text.Substring(txt2.Text.Length - 1, 1).Trim();
                if (last== "."|| last == "＋" || last == "×" || last == "÷" )
                {
                    txt2.Text = txt2.Text.Remove(txt2.Text.Length - 1, 1) + btndian.Text;//替换
                }
                if (last == "－")
                {
                    txt2.Text = txt2.Text+ 0 + btndian.Text;//作负号处理
                }
                
            }
            else { txt2.Text = 0 + btndian.Text; }
        }


        private void btnjia_Click(object sender, EventArgs e)
        {
            if (txt2.Text != "")
            {
                noRepet(btnjia.Text);
            }
        }

        private void noRepet(string btnNow)
        {
            i = 0;
            string last = txt2.Text.Substring(txt2.Text.Length - 1, 1);
            if (last == "＋" || last == "－" || last == "×" || last == "÷" || last == "-")
            {
                txt2.Text = txt2.Text.Remove(txt2.Text.Length - 1, 1);
                if (txt2.Text!="")
                {
                    txt2.Text = txt2.Text+ btnNow;//替换
                }
            }
            else
            {
                txt2.Text = txt2.Text + btnNow;//直接追加
            }
        }

        private void btnjian_Click(object sender, EventArgs e)
        {
            if (txt2.Text != "")
            {
                noRepet(btnjian.Text);
            }
            else {
                txt2.Text = txt2.Text +"-";
            }
            
        }

        private void btncheng_Click(object sender, EventArgs e)
        {
            if (txt2.Text != "")
            {
                noRepet(btncheng.Text);
            }
        }

        private void btnchu_Click(object sender, EventArgs e)
        {
            if (txt2.Text != "")
            {
                noRepet(btnchu.Text);
            }
           
        }
        private void btnmo_Click(object sender, EventArgs e)
        {
        }
        private void btnback_Click(object sender, EventArgs e)
        {
            if (txt2.Text != "") { txt2.Text = txt2.Text.Remove(txt2.Text.Length - 1, 1); }
        }
        private void btnclear_Click(object sender, EventArgs e)
        {
            if (txt2.Text != "") { txt2.Text = ""; }
            else { txt1.Text = ""; }
        }

        private void btndeng_Click(object sender, EventArgs e)
        {
            splitnum("＋");
            splitnum("－");
            splitnum("×");
            splitnum("÷");
            i = 1;
        }

        private void btndeng_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void splitnum(string f)
        {
            int position = txt2.Text.LastIndexOf(f);
            int n = txt2.Text.Length;
            if (position > 0)
            {
                if (txt2.Text.Substring(position-1,1) == ".")
                {
                    num1 = Convert.ToInt32(txt2.Text.Substring(0, position-1));
                }
                else { num1 = Convert.ToInt32(txt2.Text.Substring(0, position)); }
                 num2 = int.Parse(txt2.Text.Substring(position + 1));
                switch (f)
                {
                    case "＋":
                        txt2.Text = (num1 + num2).ToString();
                        break;
                    case "－":
                        txt2.Text = (num1-num2).ToString();
                        break;
                    case "×":
                        txt2.Text = (num1 * num2).ToString();
                        break;
                    case "÷":
                        if (num2==0)
                        {
                            txt2.Text="∞";
                        }
                        else {
                            txt2.Text = (num1 / num2).ToString();
                        }
                        break;
                }
            }

        }
    }






}

